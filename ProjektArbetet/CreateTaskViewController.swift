//
//  CreateTaskViewController.swift
//  ProjektArbetet
//
//  Created by UffeO on 24/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit
import Firebase

class CreateTaskViewController: UIViewController, UITextFieldDelegate, modalViewControllerDelegate, ReturnToLoginViewDelegate {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var addRemoveField: UITextField!
    @IBOutlet weak var saveAsButton: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    var returnON = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        amountField.delegate = self
        addRemoveField.delegate = self
        amountField.keyboardType = .NumberPad
        addRemoveField.keyboardType = .NumberPad
        
        setShadowOn(saveAsButton)
        setShadowOn(textLabel)
        setShadowOn(cancelButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        if (returnON == "yes") {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = NSCharacterSet(charactersInString: "0123456789").invertedSet
        return string.rangeOfCharacterFromSet(invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
    
    func getAmount(number: String) {
        amountField.text = number
    }
    
    func returnToSecondNav(returnOrNot: String) {
        if (returnOrNot == "yes") {
            returnON = returnOrNot
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func cancelButton(sender: UIButton)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func setShadowOn(named: UIView)
    {
        named.layer.shadowColor = UIColor.blackColor().CGColor
        named.layer.shadowOffset = CGSizeMake(5, 5)
        named.layer.shadowRadius = 5
        named.layer.shadowOpacity = 1.0
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "amountCalc"
        {
            let destination = segue.destinationViewController as! CalculateViewController
                destination.viaSegue = textView.text
                destination.delegate = self
        }
        else if segue.identifier == "saveAs"
        {
            let destination = segue.destinationViewController as! SaveTaskViewController
                destination.toDoText = textView.text
                destination.amountText = amountField.text!
                destination.addRemoveText = addRemoveField.text!
                destination.delegate = self
        }
    }

}
