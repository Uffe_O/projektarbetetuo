//
//  LoginViewController.swift
//  ProjektArbetet
//
//  Created by UffeO on 21/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var createAccount: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var login: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setShadowOn(logoutButton)
        setShadowOn(continueButton)
        setShadowOn(createAccount)
        setShadowOn(textLabel)
        setShadowOn(login)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        // shows the logout button if you already are logged in
        if NSUserDefaults.standardUserDefaults().valueForKey("uid") != nil && CURRENT_USER.authData != nil
        {
            self.logoutButton.hidden = false
            self.continueButton.hidden = false
        }
    }
    
    @IBAction func loginButton(sender: UIButton)
    {
        
        let email = emailField.text
        let password = passwordField.text
        
        if (email!.isEmpty || password!.isEmpty)
        {
            
            // display alert message
            displayAnAlertMessage("All fields required!", titleMessage: "Error!")
            
            return
        }
        else
        {
            FIREBASE_REF.authUser(email, password: password,
                                  withCompletionBlock:{(error, authData) -> Void in
                
                if error == nil
                {
                    NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: "uid")
                    self.logoutButton.hidden = false
                    self.continueButton.hidden = false
                    self.displayAnAlertMessage("Login successful!", titleMessage: "Congratulations!")
                }
                else
                {
                    self.displayAnAlertMessage("Password or email was wrong!", titleMessage: "Error!")
                    print(error)
                }
            })
        }
        
    }
    
    @IBAction func continueButton(sender: UIButton)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func logoutButton(sender: UIButton)
    {
        CURRENT_USER.unauth()
        
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "uid")
        self.logoutButton.hidden = true
        self.continueButton.hidden = true
        emailField.text = ""
        passwordField.text = ""
    }
    
    func displayAnAlertMessage(myMessage : String, titleMessage : String){
        let alert = UIAlertController(title: titleMessage, message: myMessage, preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Ok", style: .Default, handler: nil)
        
        alert.addAction(action)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func setShadowOn(named: UIView)
    {
        named.layer.shadowColor = UIColor.blackColor().CGColor
        named.layer.shadowOffset = CGSizeMake(5, 5)
        named.layer.shadowRadius = 5
        named.layer.shadowOpacity = 1.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
