//
//  MyTableController.swift
//  ProjektArbetet
//
//  Created by UffeO on 28/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit

class MyTableController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    
    var userDefaults = NSUserDefaults.standardUserDefaults()

    var userID = NSUserDefaults.standardUserDefaults().valueForKey("uid") as! String
    var superArray = Array<Array<String>>()
    
    var indexOfTable = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if userDefaults.valueForKey(userID) != nil
        {
            // load the data
            superArray = userDefaults.valueForKey(userID) as! Array
        }
        else
        {
            let newArray = [String]()
            for _ in 1...6 {
                superArray.append(newArray)
            }
        }
        
        setShadowOn(deleteButton)
        setShadowOn(infoButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return superArray[0].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomCell
        
        cell.amount.text = superArray[2][indexPath.row]
        cell.addedRemoved.text = superArray[3][indexPath.row]
        cell.theImage.image = UIImage(named: superArray[4][indexPath.row])
        cell.date.text = superArray[5][indexPath.row]
        
        return cell
    }
    
    @IBAction func deleteButton(sender: UIButton) {
        // remove data on index
        superArray[0].removeAtIndex(indexOfTable)
        superArray[1].removeAtIndex(indexOfTable)
        superArray[2].removeAtIndex(indexOfTable)
        superArray[3].removeAtIndex(indexOfTable)
        superArray[4].removeAtIndex(indexOfTable)
        superArray[5].removeAtIndex(indexOfTable)
        
        userDefaults.setObject(superArray, forKey: userID)
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
        })
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        indexOfTable = indexPath.row
        deleteButton.hidden = false
        infoButton.hidden = false
    }
    
    func setShadowOn(named: UIView)
    {
        named.layer.shadowColor = UIColor.blackColor().CGColor
        named.layer.shadowOffset = CGSizeMake(5, 5)
        named.layer.shadowRadius = 5
        named.layer.shadowOpacity = 1.0
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showInfo"
        {
            let destination = segue.destinationViewController as! PresentInfoViewController
                destination.getTitle = superArray[0][indexOfTable]
                destination.getToDoText = superArray[1][indexOfTable]
        }
    }

}
