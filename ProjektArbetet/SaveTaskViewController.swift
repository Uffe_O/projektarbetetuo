//
//  SaveTaskViewController.swift
//  ProjektArbetet
//
//  Created by UffeO on 28/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit
import Foundation

protocol ReturnToLoginViewDelegate {
    func returnToSecondNav(returnOrNot: String)
}

class SaveTaskViewController: UIViewController {
    
    var delegate: ReturnToLoginViewDelegate! = nil
    
    @IBOutlet weak var piggyBank: UIImageView!
    @IBOutlet weak var payImage: UIImageView!
    @IBOutlet weak var retrieveImage: UIImageView!
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var textLabel: UILabel!
    
    var toDoText = ""
    var amountText = ""
    var addRemoveText = ""
    
    var chosenImage = "piggyBank"
    
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var titles = [String]()
    var toDoTexts = [String]()
    var amounts = [String]()
    var addRemoves = [String]()
    var images = [String]()
    var dates = [String]()
    
    var userID = NSUserDefaults.standardUserDefaults().valueForKey("uid") as! String
    var superArray = Array<Array<String>>()
    
    var animator: UIDynamicAnimator?
    var gravity: UIGravityBehavior?
    var collision: UICollisionBehavior?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        piggyBank.image = UIImage(named: "piggyBank")
        payImage.image = UIImage(named: "handBills")
        retrieveImage.image = UIImage(named: "dollarSacks")
        
        // check if NSUserDefaults Array exists and sets the data to the empty Arrays
        if userDefaults.valueForKey(userID) != nil
        {
            // load the data
            superArray = userDefaults.valueForKey(userID) as! Array
            titles = superArray[0]
            toDoTexts = superArray[1]
            amounts = superArray[2]
            addRemoves = superArray[3]
            images = superArray[4]
            dates = superArray[5]
        }
        else
        {
            let newArray = [String]()
            for _ in 1...6 {
                superArray.append(newArray)
            }
        }
        
        // run showButton function and check for empty textfield
        self.titleField.addTarget(self, action: #selector(SaveTaskViewController.showButton(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        setShadowOn(textLabel)
        setShadowOn(saveButton)
        setShadowOn(cancelButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showButton(sender: UITextField)
    {
        if sender.text!.isEmpty
        {
            saveButton.hidden = true
        }
        else
        {
            saveButton.hidden = false
        }
    }
    
    @IBAction func indexChanged(sender: UISegmentedControl)
    {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            chosenImage = "piggyBank"
            putShakeOnImage(self.piggyBank)
        case 1:
            chosenImage = "handBills"
            putShakeOnImage(self.payImage)
        case 2:
            chosenImage = "dollarSacks"
            putShakeOnImage(self.retrieveImage)
        default:
            break
        }
    }
    
    func putShakeOnImage(imageName: UIView)
    {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(imageName.center.x - 10, imageName.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(imageName.center.x + 10, imageName.center.y))
        imageName.layer.addAnimation(animation, forKey: "position")
    }

    @IBAction func saveButton(sender: UIButton) {
        
        // save the data
        if (chosenImage == "piggyBank" || chosenImage == "dollarSacks")
        {
            var intValue = 0.0
            if amountText != "" || addRemoveText != "" {
                intValue = Double(amountText)! - Double(addRemoveText)!
            }
            addRemoveText = "+" + addRemoveText
            addRemoveText = addRemoveText + " = \(intValue)"
        }
        else
        {
            var intValue = 0.0
            if amountText != "" || addRemoveText != ""{
                intValue = Double(amountText)! - Double(addRemoveText)!
            }
            addRemoveText = "-" + addRemoveText
            addRemoveText = addRemoveText + " = \(intValue)"
        }
        
        titles.append(titleField.text!)
        userDefaults.setObject(titles, forKey: "titleTexts")
        superArray[0] = titles
        
        toDoTexts.append(toDoText)
        userDefaults.setObject(toDoTexts, forKey: "toDoTexts")
        superArray[1] = toDoTexts
        
        amounts.append(amountText)
        userDefaults.setObject(amounts, forKey: "amountTexts")
        superArray[2] = amounts
        
        addRemoves.append(addRemoveText)
        userDefaults.setObject(addRemoves, forKey: "addRemoveTexts")
        superArray[3] = addRemoves
        
        images.append(chosenImage)
        userDefaults.setObject(images, forKey: "imageTexts")
        superArray[4] = images
        
        // gets calendar date
        let calendar = NSCalendar.currentCalendar()
        let day = calendar.component(.Day, fromDate: NSDate())
        let month = calendar.component(.Month, fromDate: NSDate())
        let year = calendar.component(.Year, fromDate: NSDate())
        dates.append("\(String(format: "%02d", day))/\(String(format: "%02d", month))/\(year)")
        userDefaults.setObject(dates, forKey: "dateTexts")
        superArray[5] = dates
        
        userDefaults.setObject(superArray, forKey: userID)
        
        delegate.returnToSecondNav("yes")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelButton(sender: UIButton)
    {
        delegate.returnToSecondNav("yes")
    }
    
    func setShadowOn(named: UIView)
    {
        named.layer.shadowColor = UIColor.blackColor().CGColor
        named.layer.shadowOffset = CGSizeMake(5, 5)
        named.layer.shadowRadius = 5
        named.layer.shadowOpacity = 1.0
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
