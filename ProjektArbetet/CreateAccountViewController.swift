//
//  CreateAccountViewController.swift
//  ProjektArbetet
//
//  Created by UffeO on 21/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var repeatField: UITextField!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var cancel: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setShadowOn(textLabel)
        setShadowOn(register)
        setShadowOn(cancel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func createAccountButton(sender: UIButton) {
        
        let email = emailField.text
        let password = passwordField.text
        let repeatedPassword = repeatField.text
        
        // check for empty fields
        
        if (email!.isEmpty || password!.isEmpty || repeatedPassword!.isEmpty){
            
            //display alert message
            
            displayMyAlertMessage("All fields are required")
            
            return
        }
        else
        {
            FIREBASE_REF.createUser(email, password: password, withValueCompletionBlock: {(error, authData) -> Void in
                
                if error == nil
                {
                    FIREBASE_REF.authUser(email, password: password, withCompletionBlock: { (error, authData) -> Void in
                       if error == nil
                       {
                        NSUserDefaults.standardUserDefaults().setValue(authData.uid, forKey: "uid")
                        
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                        else
                       {
                        self.displayMyAlertMessage("All fields are required!")
                        print(error)
                        }
                    })
                }
                else
                {
                    self.displayMyAlertMessage("All fields are required!")
                    print(error)
                }
                
            })
        }
        
        if(password != repeatedPassword){
            
            displayMyAlertMessage("Passwords do not match")
            
            return
        }
        
    }
    
    @IBAction func cancelButton(sender: UIButton)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // this is needed for alert message to show up, recieves a string text with the message
    func displayMyAlertMessage(userMessage : String){
        
        let myAlert = UIAlertController(title: "Error!", message: userMessage, preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .Default, handler: nil)
        
        myAlert.addAction(okAction)
        
        self.presentViewController(myAlert, animated: true, completion: nil)
    }
    
    func setShadowOn(named: UIView)
    {
        named.layer.shadowColor = UIColor.blackColor().CGColor
        named.layer.shadowOffset = CGSizeMake(5, 5)
        named.layer.shadowRadius = 5
        named.layer.shadowOpacity = 1.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
