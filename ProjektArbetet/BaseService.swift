//
//  BaseService.swift
//  ProjektArbetet
//
//  Created by UffeO on 27/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import Foundation
import Firebase

let BASE_URL = "https://projectapplication-uo-software.firebaseio.com"

let FIREBASE_REF = Firebase(url: BASE_URL)

var CURRENT_USER : Firebase
{
    let userID = NSUserDefaults.standardUserDefaults().valueForKey("uid") as! String
    
    let currentUser = Firebase(url: "\(FIREBASE_REF)").childByAppendingPath("users").childByAppendingPath(userID)
    
    return currentUser!
}