//
//  CalculateViewController.swift
//  ProjektArbetet
//
//  Created by UffeO on 22/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit

protocol modalViewControllerDelegate {
    func getAmount(number:String)
}

class CalculateViewController: UIViewController {
    
    var delegate: modalViewControllerDelegate! = nil
    
    @IBOutlet weak var btnInput: UILabel!
    @IBOutlet weak var btnResult: UILabel!
    @IBOutlet weak var labelTextValue: UILabel!
    
    var viaSegue = ""
    
    var result = Float()
    
    var currentNumber = Float()
    
    var currentOp = String()
    
    var input = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        currentOp = "="
        btnResult.text = "\(result)"
        btnInput.text = ""
        labelTextValue.text = viaSegue
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnNumberInput(sender: UIButton) {
        btnResult.text = "0.0"
        
        currentNumber = currentNumber * 10 + Float(sender.titleLabel!.text!)!
        input += "\(sender.titleLabel!.text!)"
        btnInput.text = input + ""
        
    }

    @IBAction func btnOperation(sender: UIButton) {
        
        if btnInput.text == ""
        {
            print("no number")
        }
        else
        {
            switch currentOp
            {
            case "=":
                result = currentNumber
            case "+":
                result = result + currentNumber
            case "-":
                result = result - currentNumber
            case "*":
                result = result * currentNumber
            case "/":
                result = result / currentNumber
            default:
                print("error")
            }
        
        currentNumber = 0
        
        currentOp = sender.titleLabel!.text! as String!
        
        if sender.titleLabel!.text == "=" {
            btnResult.text = "\(result)"
            input = ""
            btnInput.text = ""
        }
        else
        {
            input += currentOp
            btnInput.text = input + ""
        }
          
        }
        
    }
    
    @IBAction func btnClear(sender: UIButton) {
        result = 0
        currentNumber = 0
        currentOp = "="
        btnResult.text = "\(result)"
        btnInput.text = ""
        input = ""
    }
    
    @IBAction func doneButton(sender: UIButton)
    {
        delegate.getAmount("\(result)")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation

    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) 
     {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "costCalc"
        {
            let destination = segue.destinationViewController as!CreateTaskViewController
                destination.viaSegueString = "\(result)"
        }
        
    }
 */

}
