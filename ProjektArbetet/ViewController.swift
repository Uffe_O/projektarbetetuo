//
//  ViewController.swift
//  ProjektArbetet
//
//  Created by UffeO on 21/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var labelTextHead: UILabel!
    @IBOutlet weak var createTaskButton: UIButton!
    @IBOutlet weak var savesButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if NSUserDefaults.standardUserDefaults().valueForKey("uid") != nil && CURRENT_USER.authData != nil
        {
            let alert = UIAlertController(title: "Alert", message: "You are currently logged in!", preferredStyle: .Alert)
            
            let action = UIAlertAction(title: "Ok", style: .Default, handler: nil)
            
            alert.addAction(action)
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            self.performSegueWithIdentifier("loginView", sender: self)
        }
        
        setShadowOn(labelTextHead)
        setShadowOn(savesButton)
        setShadowOn(logoutButton)
        setShadowOn(createTaskButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if NSUserDefaults.standardUserDefaults().valueForKey("uid") != nil && CURRENT_USER.authData != nil
        {
            self.labelTextHead.hidden = false
            self.createTaskButton.hidden = false
            self.savesButton.hidden = false
            self.logoutButton.hidden = false
        }
        else
        {
            self.labelTextHead.hidden = true
            self.createTaskButton.hidden = true
            self.savesButton.hidden = true
            self.logoutButton.hidden = true
        }
    }

    @IBAction func logoutButton(sender: UIButton) {
        
        CURRENT_USER.unauth()
        
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "uid")
        
        self.performSegueWithIdentifier("loginView", sender: self)
    }
    
    func setShadowOn(named: UIView)
    {
        named.layer.shadowColor = UIColor.blackColor().CGColor
        named.layer.shadowOffset = CGSizeMake(5, 5)
        named.layer.shadowRadius = 5
        named.layer.shadowOpacity = 1.0
    }

}

