//
//  WelcomeViewController.swift
//  ProjektArbetet
//
//  Created by UffeO on 30/04/16.
//  Copyright © 2016 UffeO. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var welcomeImage: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setShadowOn(nextButton)
        setShadowOn(infoButton)
        setShadowOn(welcomeLabel)
        
        if NSUserDefaults.standardUserDefaults().valueForKey("uid") != nil && CURRENT_USER.authData != nil
        {
            welcomeLabel.text = "Welcome back!"
        }
        else
        {
            welcomeLabel.text = "Welcome!"
        }
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 38, height: 38))
        imageView.contentMode = .ScaleAspectFit
        imageView.image = UIImage(named: "dollarHouse")
        self.navigationItem.titleView = imageView
        
        welcomeImage.image = UIImage(named: "dollarHand")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setShadowOn(named: UIView)
    {
        named.layer.shadowColor = UIColor.blackColor().CGColor
        named.layer.shadowOffset = CGSizeMake(5, 5)
        named.layer.shadowRadius = 5
        named.layer.shadowOpacity = 1.0
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
